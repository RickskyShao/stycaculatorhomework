// import logo from './logo.svg';
import './App.css';

import ButtonList from './containers/ButtonList';

function App() {
  return (
    <div className='calculator'>
        <ButtonList className='button-wrapper' />
    </div>
  );
}

export default App;
