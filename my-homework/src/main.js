const { app, BrowserWindow } = require('electron')
const path = require('path')
const isDev = require('electron-is-dev')

function loadWindow(window) {
    const urlLocation = isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, './dist/index.html')}`;
    window.loadURL(urlLocation);
    window.center();
}

function createWindow () {
    const win = new BrowserWindow({
      width: 232,
      height: 320,
      resizable: false
    })
    loadWindow(win);
}

// 应用生命周期事件
app.on('ready', () => {
    createWindow();
})

app.on('activate', function () {
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
})

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') app.quit();
})

