import React from 'react';
import DisplayBar from '../DisplayBar';

import './index.css';

class ButtonList extends React.Component {
    constructor() {
        super();
        this.state = {
            numStack: [],
            preSign: '+',
            cur: '0',
            text: ''
        };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        const val = e.target.value;
        if (val === "AC") {
            this.setState({
                numStack: [],
                preSign: '+',
                cur: '0',
                text: ''
            })
        } else if (val === "+/-") {
            let num = parseFloat(this.state.cur);
            num = 0 - num;
            this.setState({
                cur: num.toString(),
                text: num.toString()
            })
        } else if (val === "%") {
            let num = parseFloat(this.state.cur) / 100;
            this.setState({
                cur: num.toString(),
                text: num.toString()
            })
        } else if (val === "÷" || val === "×" || val === "-" || val === "+" || val === "=") {
            let numStk = this.state.numStack;
            let cur = this.state.cur;
            switch (this.state.preSign) {
                case '+':
                    numStk.push(cur);
                    break;
                case '-':
                    let num = parseFloat(cur);
                    num = 0 - num;
                    numStk.push(num.toString());
                    break;
                case '×':
                    let num1 = parseFloat(numStk.pop());
                    let num2 = parseFloat(cur);
                    let res = num1 * num2;
                    numStk.push(res.toString());
                    break;
                case '÷':
                    let numL = parseFloat(numStk.pop());
                    let numR = parseFloat(cur);
                    let res2 = numL / numR;
                    numStk.push(res2.toString());
                    break;
            }
            let ans = 0.0;
            numStk.forEach((item) => {
                let tmp = parseFloat(item);
                ans += tmp;
            })
            console.log(numStk);

            this.setState({
                numStack: numStk,
                cur : '',
                preSign: val,
                text: ans.toString(),
            })
        } else if (val === ".") {
            if (this.state.cur.indexOf('.') === -1) {
                this.setState({
                    cur: this.state.cur + val,
                    text: this.state.cur + val,
                })
            }
        } else {
            console.log(`click num:${val}`);
            this.setState({
                cur: this.state.cur === '0' ? val : this.state.cur + val,
                text: this.state.cur === '0' ? val : this.state.cur + val,
            })
        }
    }
    
    render() {
        
        return (
            <div>
                <DisplayBar className='display-bar' state={this.state}/>
                <table className="buttons">
                    <tr>
                        <td><input className="button-AC" type="button" value="AC" onClick={this.handleClick}></input></td>
                        <td><input className="button-neg" type="button" value="+/-" onClick={this.handleClick}></input></td>                   
                        <td><input className="button-percent" type="button" value="%" onClick={this.handleClick}></input></td>
                        <td><input className="button-divide" type="button" value="&divide;" onClick={this.handleClick}></input></td>           
                    </tr>
                    <tr>
                        <td><input className="button-7" type="button" value="7" onClick={this.handleClick}></input></td>
                        <td><input className="button-8" type="button" value="8" onClick={this.handleClick}></input></td>                   
                        <td><input className="button-9" type="button" value="9" onClick={this.handleClick}></input></td>
                        <td><input className="button-times" type="button" value="&times;" onClick={this.handleClick}></input></td>           
                    </tr>
                    <tr>
                        <td><input className="button-4" type="button" value="4" onClick={this.handleClick}></input></td>
                        <td><input className="button-5" type="button" value="5" onClick={this.handleClick}></input></td>                   
                        <td><input className="button-6" type="button" value="6" onClick={this.handleClick}></input></td>
                        <td><input className="button-minus" type="button" value="-" onClick={this.handleClick}></input></td>           
                    </tr>
                    <tr>
                        <td><input className="button-1" type="button" value="1" onClick={this.handleClick}></input></td>
                        <td><input className="button-2" type="button" value="2" onClick={this.handleClick}></input></td>                   
                        <td><input className="button-3" type="button" value="3" onClick={this.handleClick}></input></td>
                        <td><input className="button-plus" type="button" value="+" onClick={this.handleClick}></input></td>           
                    </tr>
                    <tr>
                        <td colSpan="2"><input className="button-0" type="button" value="0" onClick={this.handleClick}></input></td>
        
                        <td><input className="button-dot" type="button" value="." onClick={this.handleClick}></input></td>
                        <td><input className="button-equal" type="button" value="=" onClick={this.handleClick}></input></td>           
                    </tr>
                </table>
            </div>
        );
    }
}

export default ButtonList;