import { observer } from 'mobx-react';
import React from 'react';

import './index.css';

class DisplayBar extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { state } = this.props;
        return (
            <div className="display" type="text">{state.text}</div>
        );
    }
}


export default DisplayBar